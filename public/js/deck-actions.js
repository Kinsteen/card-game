function addCardToDeck(titleText, descText, moralText) {
    var deck = document.querySelector('#cards-deck');

    var template = document.querySelector("#template-card-deck");
    var clone = document.importNode(template.content, true);
    var title = clone.querySelector(".card-text-title");
    var description = clone.querySelector(".card-text-description");
    var moral = clone.querySelector(".card-moral > p");

    title.textContent = titleText;
    description.textContent = descText;
    moral.textContent = moralText;

    let child = clone.children[0];

    child.style.left = '50%';
    child.style.top = '-30rem';
    child.style.transform = 'translateX(-50%)';
    child.style.opacity = '0';
    child.style.zIndex = 9999;

    deck.appendChild(clone);
    
    setupUseButton(child.querySelector('.button-card'));

    removeAllAnimations();

    setTimeout(() => {
        child.style.opacity = '1';
    }, 10);

    setTimeout(() => {
        setupAnimation();
    }, 2010);
}

function setupUseButton(element) {
    element.addEventListener('click', (e) => {
        var cardUsed = e.target.closest('.card');

        cardUsed.style.left = '50%';
        cardUsed.style.top = '-50rem';
        cardUsed.style.zIndex = 9999;
    
        removeAllAnimations();
    
        setTimeout(() => {
            cardUsed.style.opacity = '0';
        }, 500);
    
        setTimeout(() => {
            cardUsed.remove();
            setupAnimation();
        }, 800);
    });
}

function setupAllUseButtons() {
    var buttons = document.querySelectorAll('.button-card');

    for (let i = 0; i < buttons.length; i++) {
        const element = buttons[i];

        setupUseButton(element);
    }
}

var cardsUseHandlers = [];

setupAllUseButtons();