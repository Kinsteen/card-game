function deckLeave(e) {
    var cardsInDeck = document.querySelectorAll("#cards-deck > .card");

    for (let i = 0; i < cardsInDeck.length; i++) {
        const element = cardsInDeck[i];
        element.style.top = topOffset;
        element.style.transform = 'translate(-50%, 0) rotate(' + (i - (cardsInDeck.length-1)/2) + 'deg)';
    }
}

function cardHover(e, i) {
    var cardsInDeck = document.querySelectorAll("#cards-deck > .card");

    var cardClicked = e.target.closest('.card');
    
    cardClicked.classList.remove('opacity-80');
    cardClicked.style.zIndex = 999;
    cardClicked.style.transform = 'translate(-50%, 0) rotate(0deg)';
    cardClicked.style.top = topOffsetSelected;

    for (let j = 0; j < cardsInDeck.length; j++) {
        const otherCard = cardsInDeck[j];
        
        if (otherCard != cardClicked) {
            if (j < i) {
                otherCard.style.zIndex = j;
            } else {
                otherCard.style.zIndex = cardsInDeck.length - 1 - j;
            }

            otherCard.classList.add('opacity-80');
            otherCard.style.top = -12 - 5/Math.abs(j-i) + 'rem';
            otherCard.style.transform = 'translate(-50%, 0) rotate(' + (j-i)*1 + 'deg)';
        }
    }
}

function setupAnimation() {
    var cardsInDeck = document.querySelectorAll("#cards-deck > .card");
    var deck = document.querySelector('#cards-deck');

    deck.removeEventListener('mouseleave', deckLeave);

    deck.addEventListener('mouseleave', deckLeave);
    
    for (let i = 0; i < cardsInDeck.length; i++) {
        const element = cardsInDeck[i];
    
        var baseOffset = 2;
        let remCalc = ((i - (cardsInDeck.length-1)/2)*baseOffset)+(1/cardsInDeck.length)*20*(i - (cardsInDeck.length-1)/2);
        element.style.left = 'calc(50% + ' + remCalc + 'rem)';
        element.style.transform = 'translate(-50%, 0) rotate('+ (i - (cardsInDeck.length-1)/2) + 'deg)';
        element.style.top = topOffset;

        element.removeEventListener('mouseenter', (e) => {cardHover(e, i)});
        
        let handler = (e) => {cardHover(e, i)};
        cardsAnimationHandlers.push({element: element, handler: handler});
        element.addEventListener('mouseenter', handler);
    }
}

function removeAllAnimations() {
    var deck = document.querySelector('#cards-deck');

    deck.removeEventListener('mouseleave', deckLeave);

    cardsAnimationHandlers.forEach(obj => {
        obj.element.removeEventListener('mouseenter', obj.handler);
    });
}

var topOffset = '1rem';
var topOffsetSelected = '-24rem';

var cardsAnimationHandlers = [];

setupAnimation();
