const express = require('express')
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
    socket.broadcast.emit('new user connected');

    socket.on('disconnect', () => {
        socket.broadcast.emit('user disconnected');
    });

    socket.on('chat message', (username, msg) => {
        io.emit('chat message', username, msg);
    });
});

app.use(express.static('public')); // Public assets

http.listen(3000, () => {
    console.log('listening on *:3000');
});